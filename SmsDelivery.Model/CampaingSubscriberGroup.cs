//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmsDelivery.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class CampaingSubscriberGroup
    {
        public int Id { get; set; }
        public int CampaignId { get; set; }
        public int GroupId { get; set; }
    
        public virtual Campaing Campaing { get; set; }
    }
}
