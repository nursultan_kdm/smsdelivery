﻿namespace SmsDelivery.Model.Enums
{
    public enum CampaignStatus
    {
        New,
        Scheduled,
        InProgress,
        Finished,
        Paused,
        Canceled
    }
}