﻿namespace SmsDelivery.Model.Enums
{
    public enum CampaignTypes
    {
        Sms,
        Email,
        Viber
    }
}