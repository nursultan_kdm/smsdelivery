﻿namespace SmsDelivery.Model.Enums
{
    public enum PaymentStatus
    {
        New = 1, //Новый
        Pending = 2, //Обрабатывается
        Processed = 3, //Обработан
        Failed = 4 //Ошибка
    }
}