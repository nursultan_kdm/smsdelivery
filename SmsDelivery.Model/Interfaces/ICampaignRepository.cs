﻿using System.Collections.Generic;

namespace SmsDelivery.Model.Interfaces
{
    public interface ICampaignRepository : IRepository<Campaing>
    {
        IEnumerable<CampaignType> GetCampaignTypes();
    }
}