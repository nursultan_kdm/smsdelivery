﻿namespace SmsDelivery.Model.Interfaces
{
    public interface IPaymentInstrumentsRepository : IRepository<PaymentInstrument>
    {
        
    }
}