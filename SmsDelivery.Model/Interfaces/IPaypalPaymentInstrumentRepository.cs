﻿namespace SmsDelivery.Model.Interfaces
{
    public interface IPaypalPaymentInstrumentRepository : IRepository<PaypalPaymentInstrument>
    {

    }
}