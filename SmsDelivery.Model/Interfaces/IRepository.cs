﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SmsDelivery.Model.Interfaces
{
    public interface IRepository<T> : IDisposable
    {
        IQueryable<T> GetAll();
        T GetById(object id);
        void Insert(T obj);
        void Update(T obj);
        void Delete(Object id);
        void Save();
    }
}