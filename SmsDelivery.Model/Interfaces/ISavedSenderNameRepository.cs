﻿namespace SmsDelivery.Model.Interfaces
{
    public interface ISavedSenderNameRepository : IRepository<SavedSenderName>
    {
        
    }
}