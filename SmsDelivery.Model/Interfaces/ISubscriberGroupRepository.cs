﻿

namespace SmsDelivery.Model.Interfaces
{
    public interface ISubscriberGroupRepository : IRepository<SubscriberGroup>
    {
        
    }
}