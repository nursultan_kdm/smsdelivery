﻿

namespace SmsDelivery.Model.Interfaces
{
    public interface ISubscriberRepository : IRepository<Subscriber>
    {
        
    }
}