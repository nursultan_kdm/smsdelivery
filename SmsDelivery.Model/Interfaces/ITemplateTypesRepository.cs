﻿namespace SmsDelivery.Model.Interfaces
{
    public interface ITemplateTypesRepository : IRepository<TemplateType>
    {
        
    }
}