﻿using System.ComponentModel.DataAnnotations;

namespace SmsDelivery.Model
{
    [MetadataType(typeof(SubscriberMeta))]
    public partial class Subscriber
    {
        
    }

    public class SubscriberMeta
    {
        public int Id { get; set; }
        
        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        public string Phone { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public int CountryId { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int SubscriberGroupId { get; set; }
    }
}