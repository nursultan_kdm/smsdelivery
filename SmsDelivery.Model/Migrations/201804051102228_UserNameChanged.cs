namespace SmsDelivery.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserNameChanged : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "UserName", c => c.String());
            DropColumn("dbo.Customers", "Login");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Customers", "Login", c => c.String());
            DropColumn("dbo.Customers", "UserName");
        }
    }
}
