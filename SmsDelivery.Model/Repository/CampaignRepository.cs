﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SmsDelivery.Model.Interfaces;

namespace SmsDelivery.Model.Repository
{
    public class CampaignRepository : ICampaignRepository
    {
        private SmsDeliveryDbEntities context;

        public CampaignRepository()
        {
            context = new SmsDeliveryDbEntities();
        }

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IQueryable<Campaing> GetAll()
        {
            return context.Campaings;
        }

        public Campaing GetById(object id)
        {
            return context.Campaings.Find(id);
        }

        public void Insert(Campaing obj)
        {
            context.Campaings.Add(obj);
        }

        public void Update(Campaing obj)
        {
            context.Entry(obj).State = EntityState.Modified;
        }

        public void Delete(object id)
        {
            var objToDelete = context.Campaings.Find(id);
            context.Campaings.Remove(objToDelete);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public IEnumerable<CampaignType> GetCampaignTypes()
        {
            return context.CampaignTypes.AsEnumerable();
        }
    }
}