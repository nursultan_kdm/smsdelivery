﻿using System;
using System.Data.Entity;
using System.Linq;
using SmsDelivery.Model.Interfaces;

namespace SmsDelivery.Model.Repository
{
    public class CampaignSubscriberGroupsRepository : ICampaignSubscribersGroupsRepository
    {
        private SmsDeliveryDbEntities context;

        public CampaignSubscriberGroupsRepository()
        {
            context = new SmsDeliveryDbEntities();
        }

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IQueryable<CampaingSubscriberGroup> GetAll()
        {
            return context.CampaingSubscriberGroups;
        }

        public CampaingSubscriberGroup GetById(object id)
        {
            return context.CampaingSubscriberGroups.Find(id);
        }

        public void Insert(CampaingSubscriberGroup obj)
        {
            context.CampaingSubscriberGroups.Add(obj);
        }

        public void Update(CampaingSubscriberGroup obj)
        {
            context.Entry(obj).State = EntityState.Modified;
        }

        public void Delete(object id)
        {
            var objToDelete = context.CampaingSubscriberGroups.Find(id);
            context.CampaingSubscriberGroups.Remove(objToDelete);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}