﻿using System;
using System.Data.Entity;
using System.Linq;
using SmsDelivery.Model.Interfaces;

namespace SmsDelivery.Model.Repository
{
    public class CountryRepository : ICountryRepository
    {
        private SmsDeliveryDbEntities context;

        public CountryRepository()
        {
            context = new SmsDeliveryDbEntities();
        }

        private bool _disposed;

        protected void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IQueryable<Country> GetAll()
        {
            return context.Countries;
        }

        public Country GetById(object id)
        {
            return context.Countries.Find(id);
        }

        public void Insert(Country obj)
        {
            context.Countries.Add(obj);
        }

        public void Update(Country obj)
        {
            context.Entry(obj).State = EntityState.Modified;
        }

        public void Delete(object id)
        {
            var countryToRemove = context.Countries.Find(id);
            context.Countries.Remove(countryToRemove);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}