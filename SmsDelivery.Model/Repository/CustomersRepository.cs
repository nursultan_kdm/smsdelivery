﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SmsDelivery.Model.Interfaces;

namespace SmsDelivery.Model.Repository
{
    public class CustomersRepository : ICustomersRepository
    {
        
        private SmsDeliveryDbEntities context;

        public CustomersRepository()
        {
            context = new SmsDeliveryDbEntities();
        }

        public IQueryable<Customer> GetAll()
        {
            return context.Customers;
        }

        public Customer GetById(object id)
        {
            return context.Customers.Find(id);
        }

        public void Insert(Customer obj)
        {
            context.Customers.Add(obj);
        }
        public void Update(Customer obj)
        {
            context.Entry(obj).State = EntityState.Modified;
        }
        public void Delete(object id)
        {
            Customer getObjById = context.Customers.Find(id);
            context.Customers.Remove(getObjById);
        }
        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    }
}