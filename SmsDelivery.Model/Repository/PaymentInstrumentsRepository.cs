﻿using System;
using System.Data.Entity;
using System.Linq;
using SmsDelivery.Model.Interfaces;

namespace SmsDelivery.Model.Repository
{
    public class PaymentInstrumentsRepository : IPaymentInstrumentsRepository
    {
        private SmsDeliveryDbEntities context;

        public PaymentInstrumentsRepository()
        {
            context = new SmsDeliveryDbEntities();
        }

        private bool _disposed;

        protected void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            _disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IQueryable<PaymentInstrument> GetAll()
        {
            return context.PaymentInstruments;
        }

        public PaymentInstrument GetById(object id)
        {
            return context.PaymentInstruments.Find(id);
        }

        public void Insert(PaymentInstrument obj)
        {
            context.PaymentInstruments.Add(obj);
        }

        public void Update(PaymentInstrument obj)
        {
            context.Entry(obj).State = EntityState.Modified;
        }

        public void Delete(object id)
        {
            var objToDelete = context.PaymentInstruments.Find(id);
            context.PaymentInstruments.Remove(objToDelete);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}