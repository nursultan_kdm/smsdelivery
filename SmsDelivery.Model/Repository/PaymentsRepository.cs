﻿using System;
using System.Data.Entity;
using System.Linq;
using SmsDelivery.Model.Interfaces;

namespace SmsDelivery.Model.Repository
{
    public class PaymentsRepository : IPaymentsRepository
    {
        private SmsDeliveryDbEntities context;

        public PaymentsRepository()
        {
            context = new SmsDeliveryDbEntities();
        }

        private bool _disposed;

        protected void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IQueryable<Payment> GetAll()
        {
            return context.Payments;
        }

        public Payment GetById(object id)
        {
            return context.Payments.Find(id);
        }

        public void Insert(Payment obj)
        {
            context.Payments.Add(obj);
        }

        public void Update(Payment obj)
        {
            context.Entry(obj).State = EntityState.Modified;
        }

        public void Delete(object id)
        {
            var objToDelete = context.Payments.Find(id);
            context.Payments.Remove(objToDelete);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}