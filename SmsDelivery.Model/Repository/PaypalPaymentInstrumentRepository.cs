﻿using System;
using System.Data.Entity;
using System.Linq;
using SmsDelivery.Model.Interfaces;

namespace SmsDelivery.Model.Repository
{
    public class PaypalPaymentInstrumentRepository : IPaypalPaymentInstrumentRepository
    {
        private SmsDeliveryDbEntities context;

        public PaypalPaymentInstrumentRepository()
        {
            context = new SmsDeliveryDbEntities();
            
        }

        private bool _disposed;

        protected void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IQueryable<PaypalPaymentInstrument> GetAll()
        {
            return context.PaypalPaymentInstruments;
        }

        public PaypalPaymentInstrument GetById(object id)
        {
            return context.PaypalPaymentInstruments.Find(id);
        }

        public void Insert(PaypalPaymentInstrument obj)
        {
            context.PaypalPaymentInstruments.Add(obj);
        }

        public void Update(PaypalPaymentInstrument obj)
        {
            context.Entry(obj).State = EntityState.Modified;
        }

        public void Delete(object id)
        {
            var objToDelete = context.PaypalPaymentInstruments.Find(id);
            context.PaypalPaymentInstruments.Remove(objToDelete);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}