﻿using System;
using System.Data.Entity;
using System.Linq;
using SmsDelivery.Model.Interfaces;

namespace SmsDelivery.Model.Repository
{
    public class SavedSenderNameRepository : ISavedSenderNameRepository
    {
        private SmsDeliveryDbEntities context;

        public SavedSenderNameRepository()
        {
            context = new SmsDeliveryDbEntities();
        }

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            _disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IQueryable<SavedSenderName> GetAll()
        {
            return context.SavedSenderNames;
        }

        public SavedSenderName GetById(object id)
        {
            return context.SavedSenderNames.Find(id);
        }

        public void Insert(SavedSenderName obj)
        {
            context.SavedSenderNames.Add(obj);
        }

        public void Update(SavedSenderName obj)
        {
            context.Entry(obj).State = EntityState.Modified;
        }

        public void Delete(object id)
        {
            var objToDelete = context.SavedSenderNames.Find(id);
            context.SavedSenderNames.Remove(objToDelete);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}