﻿using System;
using System.Data.Entity;
using System.Linq;
using SmsDelivery.Model.Interfaces;

namespace SmsDelivery.Model.Repository
{
    public class SubscriberGroupRepository : ISubscriberGroupRepository
    {
        private SmsDeliveryDbEntities context;

        public SubscriberGroupRepository()
        {
            context = new SmsDeliveryDbEntities();
        }

        private bool disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IQueryable<SubscriberGroup> GetAll()
        {
            return context.SubscriberGroups;
        }

        public SubscriberGroup GetById(object id)
        {
            return context.SubscriberGroups.Find(id);
        }

        public void Insert(SubscriberGroup obj)
        {
            context.SubscriberGroups.Add(obj);
        }

        public void Update(SubscriberGroup obj)
        {
            context.Entry(obj).State = EntityState.Modified;
        }

        public void Delete(object id)
        {
            SubscriberGroup getObjById = context.SubscriberGroups.Find(id);
            context.SubscriberGroups.Remove(getObjById);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}