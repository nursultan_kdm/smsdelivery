﻿using System;
using System.Data.Entity;
using System.Linq;
using SmsDelivery.Model.Interfaces;

namespace SmsDelivery.Model.Repository
{
    public class SubscriberRepository : ISubscriberRepository
    {
        private SmsDeliveryDbEntities context;

        public SubscriberRepository()
        {
            context = new SmsDeliveryDbEntities();
        }

        private bool disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IQueryable<Subscriber> GetAll()
        {
            return context.Subscribers;
        }

        public Subscriber GetById(object id)
        {
            return context.Subscribers.Find(id);
        }

        public void Insert(Subscriber obj)
        {
            context.Subscribers.Add(obj);
        }

        public void Update(Subscriber obj)
        {
            context.Entry(obj).State = EntityState.Modified;
        }

        public void Delete(object id)
        {
            Subscriber getObjById = context.Subscribers.Find(id);
            context.Subscribers.Remove(getObjById);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}