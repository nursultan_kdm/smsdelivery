﻿using System;
using System.Data.Entity;
using System.Linq;
using SmsDelivery.Model.Interfaces;

namespace SmsDelivery.Model.Repository
{
    public class TemplateTypesRepository : ITemplateTypesRepository
    {
        private SmsDeliveryDbEntities context;

        public TemplateTypesRepository()
        {
            context = new SmsDeliveryDbEntities();
        }

        private bool _disposed;

        protected void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            _disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IQueryable<TemplateType> GetAll()
        {
            return context.TemplateTypes;
        }

        public TemplateType GetById(object id)
        {
            return context.TemplateTypes.Find(id);
        }

        public void Insert(TemplateType obj)
        {
            context.TemplateTypes.Add(obj);
        }

        public void Update(TemplateType obj)
        {
            context.Entry(obj).State = EntityState.Modified;
        }

        public void Delete(object id)
        {
            var objToDelete = context.TemplateTypes.Find(id);
            context.TemplateTypes.Remove(objToDelete);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}