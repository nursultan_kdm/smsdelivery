﻿using System;
using System.Data.Entity;
using System.Linq;
using SmsDelivery.Model.Interfaces;

namespace SmsDelivery.Model.Repository
{
    public class TemplatesRepository : ITemplatesRepository
    {
        private SmsDeliveryDbEntities context;

        public TemplatesRepository()
        {
            context = new SmsDeliveryDbEntities();
        }

        private bool _disposed;

        protected void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IQueryable<Template> GetAll()
        {
            return context.Templates;
        }

        public Template GetById(object id)
        {
            return context.Templates.Find(id);
        }

        public void Insert(Template obj)
        {
            context.Templates.Add(obj);
        }

        public void Update(Template obj)
        {
            context.Entry(obj).State = EntityState.Modified;
        }

        public void Delete(object id)
        {
            var objToDelete = context.Templates.Find(id);
            context.Templates.Remove(objToDelete);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}