﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmsDelivery.Model
{
    public class SmsDeliveryContext : DbContext
    {
        public SmsDeliveryContext() : base("name=SmsDeliveryDb")
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<SmsDeliveryContext>());
        }

        public DbSet<Country> Countries { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Subscriber> Subscribers { get; set; }
        public DbSet<SubscriberGroup> SubscriberGroups { get; set; }

        public static SmsDeliveryContext Create()
        {
            return new SmsDeliveryContext();
        }
    }
}
