﻿CREATE TABLE [dbo].[Administrators] (
    [Id]            NVARCHAR (128) NOT NULL,
    [Email]         NVARCHAR (255) NOT NULL,
    [UserName]      NVARCHAR (255) NOT NULL,
    [SecurityStamp] NVARCHAR (128) NOT NULL,
    [PasswordHash]  NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_Administrators] PRIMARY KEY CLUSTERED ([Id] ASC)
);

