﻿CREATE TABLE [dbo].[CampaingSubscriberGroups]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [CampaignId] INT NOT NULL, 
    [GroupId] INT NOT NULL, 
    CONSTRAINT [FK_CampaingSubscriberGroups_Campaign] FOREIGN KEY ([CampaignId]) REFERENCES [Campaings]([Id])
)
