﻿CREATE TABLE [dbo].[Campaings]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(255) NOT NULL, 
    [Body] NVARCHAR(MAX) NULL, 
    [TemplateId] INT NULL, 
    [CreatedDate] DATETIME NOT NULL, 
    [ProcessedDate] DATETIME NULL, 
    [ScheduledDate] DATETIME NULL, 
    [LimitOfMessages] INT NULL, 
    [ScheduledDaysOfWeek] NVARCHAR(50) NULL, 
    [CustomerId] NVARCHAR(128) NOT NULL, 
    [Status] INT NOT NULL, 
    [CampaignTypeId] INT NOT NULL, 
    [Cost] DECIMAL(18, 2) NOT NULL, 
    CONSTRAINT [FK_Campaings_CampaignType] FOREIGN KEY ([CampaignTypeId]) REFERENCES [CampaignTypes]([Id]), 
    CONSTRAINT [FK_Campaings_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [Customers]([Id])
)
