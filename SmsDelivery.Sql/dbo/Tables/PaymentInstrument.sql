﻿CREATE TABLE [dbo].[PaymentInstrument] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [IsMain]     BIT            NOT NULL,
    [CustomerId] NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_PaymentInstrument] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_314] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([Id])
);

