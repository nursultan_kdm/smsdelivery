﻿CREATE TABLE [dbo].[Payments] (
    [Id]                  INT              IDENTITY (1, 1) NOT NULL,
    [Guid]                UNIQUEIDENTIFIER NOT NULL,
    [Amount]              DECIMAL (18, 2)  NOT NULL,
    [Description]         NVARCHAR (255)   NULL,
    [CustomerId]          NVARCHAR (128)   NOT NULL,
    [CreatedDate]         DATETIME         NOT NULL,
    [ProcessedDate]       DATETIME         NULL,
    [PaymentInstrumentId] INT              NOT NULL,
    [Status] INT NOT NULL, 
    CONSTRAINT [PK_Payments] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_346] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([Id])
);

