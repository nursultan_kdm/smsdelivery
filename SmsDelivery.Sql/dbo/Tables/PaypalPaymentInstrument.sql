﻿CREATE TABLE [dbo].[PaypalPaymentInstrument] (
    [Id]      INT NOT NULL,
    [Account] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_PaypalPaymentInstrument] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PaypalPaymentInstrument_PaymentInstrument] FOREIGN KEY ([Id]) REFERENCES [dbo].[PaymentInstrument] ([Id])
);

