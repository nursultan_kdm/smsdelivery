﻿CREATE TABLE [dbo].[PaypalPayments] (
    [Id]       INT            IDENTITY (1, 1) NOT NULL,
    [Response] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_PaypalPayments] PRIMARY KEY CLUSTERED ([Id] ASC)
);

