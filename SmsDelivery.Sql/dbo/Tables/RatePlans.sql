﻿CREATE TABLE [dbo].[RatePlans] (
    [Id]             INT            IDENTITY (1, 1) NOT NULL,
    [Name]           NVARCHAR (255) NOT NULL,
    [LastUpdate]     DATETIME       NOT NULL,
    [CurrencyId]     INT            NOT NULL,
    [RatePlanTypeId] INT            NOT NULL,
    [CreatedDate]    DATETIME       NOT NULL,
    CONSTRAINT [PK_RatePlans] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_132] FOREIGN KEY ([RatePlanTypeId]) REFERENCES [dbo].[RatePlanTypes] ([Id]),
    CONSTRAINT [FK_163] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([Id])
);

