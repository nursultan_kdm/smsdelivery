﻿CREATE TABLE [dbo].[Resellers] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [CompanyName] NVARCHAR (255) NOT NULL,
    [Email]       NVARCHAR (255) NOT NULL,
    [Address]     NVARCHAR (255) NOT NULL,
    [Phone]       NVARCHAR (50)  NOT NULL,
    [Fax]         NVARCHAR (50)  NOT NULL,
    [GMT]         INT            NOT NULL,
    [Web]         NVARCHAR (255) NOT NULL,
    [CurrencyId]  INT            NOT NULL,
    [CreatedDate] DATETIME       NOT NULL,
    [UpdatedDate] DATETIME       NOT NULL,
    CONSTRAINT [PK_Resellers] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_155] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([Id])
);

