﻿CREATE TABLE [dbo].[Resellers_Countries] (
    [CountryId]  INT NOT NULL,
    [ResellerId] INT NOT NULL,
    CONSTRAINT [PK_Resellers_Countries] PRIMARY KEY CLUSTERED ([CountryId] ASC, [ResellerId] ASC),
    CONSTRAINT [FK_170] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Countries] ([Id]),
    CONSTRAINT [FK_175] FOREIGN KEY ([ResellerId]) REFERENCES [dbo].[Resellers] ([Id])
);

