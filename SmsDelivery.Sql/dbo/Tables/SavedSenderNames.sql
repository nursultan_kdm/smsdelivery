﻿CREATE TABLE [dbo].[SavedSenderNames]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(255) NOT NULL, 
    [CustomerId] NVARCHAR(128) NOT NULL, 
    CONSTRAINT [FK_SavedSenderNames_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [Customers]([Id])
)
