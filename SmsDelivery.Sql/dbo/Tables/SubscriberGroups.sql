﻿CREATE TABLE [dbo].[SubscriberGroups] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (255) NOT NULL,
    [CustomerId]  NVARCHAR (128) NOT NULL,
    [IsMain]      BIT            NOT NULL,
    [Description] NVARCHAR (500) NULL,
    CONSTRAINT [PK_SubscriberGroups] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_262] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([Id])
);

