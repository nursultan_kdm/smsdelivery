﻿CREATE TABLE [dbo].[Subscribers] (
    [Id]                INT            IDENTITY (1, 1) NOT NULL,
    [Name]              NVARCHAR (255) NOT NULL,
    [Surname]           NVARCHAR (255) NOT NULL,
    [Phone]             NVARCHAR (50)  NOT NULL,
    [Email]             NVARCHAR (255) NOT NULL,
    [Address]           NVARCHAR (255) NOT NULL,
    [City]              NVARCHAR (255) NOT NULL,
    [CountryId]         INT            NOT NULL,
    [CreatedDate]       DATETIME       NOT NULL,
    [SubscriberGroupId] INT            NOT NULL,
    CONSTRAINT [PK_Subscribers] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_354] FOREIGN KEY ([SubscriberGroupId]) REFERENCES [dbo].[SubscriberGroups] ([Id])
);

