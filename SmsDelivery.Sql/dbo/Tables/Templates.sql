﻿CREATE TABLE [dbo].[Templates]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(255) NOT NULL, 
    [Body] NVARCHAR(MAX) NOT NULL, 
    [TemplateTypeId] INT NOT NULL, 
    [CustomerId] NVARCHAR(128) NOT NULL, 
    CONSTRAINT [FK_Templates_TemplateTypes] FOREIGN KEY ([TemplateTypeId]) REFERENCES [TemplateTypes]([Id]), 
    CONSTRAINT [FK_Templates_Customers] FOREIGN KEY ([CustomerId]) REFERENCES [Customers]([Id])
)
