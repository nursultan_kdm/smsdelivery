﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmsDelivery.Controllers;
using SmsDelivery.Model;
using SmsDelivery.Model.Interfaces;
using SmsDelivery.Models;
using SmsDelivery.ViewModels;

namespace SmsDelivery.Tests.Controllers
{
    [TestClass]
    public class FinancialsTestController
    {
        private FinancialsController controller;
        public Mock<IPaymentInstrumentsRepository> paymentInstrumentRepoMock;
        private Mock<IPaymentsRepository> paymentsRepoMock;
        private Mock<IPaypalPaymentInstrumentRepository> paypalInstrumentRepoMock;

        [TestInitialize]
        public void Setup()
        {
            paymentInstrumentRepoMock = new Mock<IPaymentInstrumentsRepository>();
            paymentsRepoMock = new Mock<IPaymentsRepository>();
            paypalInstrumentRepoMock = new Mock<IPaypalPaymentInstrumentRepository>();
            controller = new FinancialsController(paymentInstrumentRepoMock.Object, paymentsRepoMock.Object, paypalInstrumentRepoMock.Object);

        }
        [TestMethod]
        public void CreatePaymentMethodTest()
        {
            var paymentInstrument = new PaymentInstrumentViewModel(){IsMain = false, Account = "t@t.ru"};
            controller.UserId = Guid.NewGuid().ToString();

            ActionResult result = controller.CreatePaymentMethod(paymentInstrument);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void CreatePaymentTest()
        {
            var payment = new Payment()
            {
                Amount = 10,
                CreatedDate = DateTime.Now,
                CustomerId = Guid.NewGuid().ToString(),
                Guid = Guid.NewGuid(),
                PaymentInstrumentId = 1
            };
            List<PaymentInstrument> paymentInstruments = new List<PaymentInstrument>()
            {
                new PaymentInstrument()
                {
                    CustomerId = "857ee67a-a138-47f1-9a6b-c0a712764b82",
                    IsMain = true,
                    Id = 1
                }
            };
            paymentInstrumentRepoMock.Setup(x => x.GetAll()).Returns(paymentInstruments.AsQueryable());

            controller.UserId = "857ee67a-a138-47f1-9a6b-c0a712764b82";

            JsonResult result = controller.CreatePayment(payment);
            
            Assert.AreEqual(((Response)result.Data).Success, true);

        }
    }
}
