﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using SmsDelivery.Model;
using SmsDelivery.Model.Enums;
using SmsDelivery.Model.Interfaces;
using SmsDelivery.Models.FilteringAndSorting;
using SmsDelivery.ViewModels;

namespace SmsDelivery.Controllers
{
    public class CampaignsController : Controller
    {
        public string UserId => User.Identity.GetUserId();
        public ICampaignRepository CampaignRepository { get; set; }
        public ICampaignSubscribersGroupsRepository CampaignSubscribersGroupsRepository { get; set; }
        public ITemplatesRepository TemplatesRepository { get; set; }
        public ISubscriberGroupRepository SubscriberGroupRepository { get; set; }
        public ISavedSenderNameRepository SavedSenderNameRepository { get; set; }

        public CampaignsController(ICampaignRepository campaignRepository, ITemplatesRepository templatesRepository, 
            ISubscriberGroupRepository subscriberGroupRepository, ISavedSenderNameRepository savedSenderNameRepository,
            ICampaignSubscribersGroupsRepository campaignSubscribersGroupsRepository)
        {
            CampaignRepository = campaignRepository;
            TemplatesRepository = templatesRepository;
            SubscriberGroupRepository = subscriberGroupRepository;
            SavedSenderNameRepository = savedSenderNameRepository;
            CampaignSubscribersGroupsRepository = campaignSubscribersGroupsRepository;
        }
        // GET: Campaigns
        public ActionResult Index(CampaignsFilteredModel campaignsFilteredModel, int? page)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;
            if (campaignsFilteredModel == null)
            {
                campaignsFilteredModel = new CampaignsFilteredModel();
            }
            if (campaignsFilteredModel.PageSize <= 0)
            {
                campaignsFilteredModel.PageSize = 10;
            }

            var campaings = CampaignRepository.GetAll();

            campaignsFilteredModel.ProcessData(campaings, "ID", true, pageNumber);
            return View(campaignsFilteredModel);
        }

        public ActionResult Create()
        {
            var model = new CampaignViewModel();
            var subscriberGroup = new SubscriberGroupViewModel();
            subscriberGroup.SubscriberGroups = new SelectList(SubscriberGroupRepository.GetAll().Where(x => x.CustomerId.Equals(UserId)).ToList(), "Id", "Name");
            model.SubscriberGroupViews.Add(subscriberGroup);

            ViewBag.Templates = new SelectList(TemplatesRepository.GetAll().Where(x => x.CustomerId.Equals(UserId)).ToList(), "Id", "Name");

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CampaignViewModel model)
        {
            if (ModelState.IsValid)
            {
                var campaign = new Campaing();
                campaign.Name = model.Name;
                campaign.CampaignTypeId = (int) CampaignTypes.Sms;
                campaign.CreatedDate = DateTime.Now;
                if (model.ScheduleDate)
                {
                    campaign.Status = (int) CampaignStatus.Scheduled;
                    campaign.ScheduledDate = model.ScheduledDate;
                }
                else
                {
                    campaign.Status = (int) CampaignStatus.New;
                }

                if (model.TemplateId.HasValue)
                {
                    campaign.TemplateId = model.TemplateId.Value;
                }

                campaign.Body = model.Body;
                campaign.Cost = 0;
                campaign.CustomerId = UserId;

                CampaignRepository.Insert(campaign);
                CampaignRepository.Save();

                foreach (var subscriberGroup in model.SubscriberGroupViews)
                {
                    var group = new CampaingSubscriberGroup();
                    group.CampaignId = campaign.Id;
                    group.GroupId = subscriberGroup.SubscriberGroupId;
                    CampaignSubscribersGroupsRepository.Insert(group);
                }
                CampaignSubscribersGroupsRepository.Save();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        public ActionResult SubscriberGroupEntryRow()
        {
            var subscriberGroup = new SubscriberGroupViewModel();
            subscriberGroup.SubscriberGroups = new SelectList(SubscriberGroupRepository.GetAll().Where(x => x.CustomerId.Equals(UserId)).ToList(), "Id", "Name");
            return PartialView("_SubscriberGroupEntry", subscriberGroup);
        }

        public JsonResult SenderNamesSearch(string query)
        {
            var names = SavedSenderNameRepository.GetAll()
                .Where(x => x.CustomerId.Equals(UserId) && x.Name.Contains(query)).Select(x => new {x.Id, x.Name}).ToList();

            return Json(names, JsonRequestBehavior.AllowGet);
        }
    }
}