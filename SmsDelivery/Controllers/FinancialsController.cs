﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using SmsDelivery.Model;
using SmsDelivery.Model.Enums;
using SmsDelivery.Model.Interfaces;
using SmsDelivery.Models;
using SmsDelivery.ViewModels;

namespace SmsDelivery.Controllers
{
    [Authorize]
    public class FinancialsController : Controller
    {
        public string UserId => User.Identity.GetUserId();
        public IPaymentsRepository PaymentsRepository { get; set; }
        public IPaymentInstrumentsRepository PaymentInstrumentsRepository { get; set; }
        public IPaypalPaymentInstrumentRepository PaypalPaymentInstrumentRepository { get; set; }

        public FinancialsController(IPaymentInstrumentsRepository paymentInstrumentsRepository,
            IPaymentsRepository paymentsRepository, IPaypalPaymentInstrumentRepository paypalPaymentInstrumentRepository)
        {
            PaymentInstrumentsRepository = paymentInstrumentsRepository;
            PaymentsRepository = paymentsRepository;
            PaypalPaymentInstrumentRepository = paypalPaymentInstrumentRepository;
        }
        // GET: Financials
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NewPayment()
        {
            return PartialView("_NewPaymentPartial");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreatePayment(Payment payment)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userPaymentMethods = PaymentInstrumentsRepository.GetAll().Where(x => x.CustomerId.Equals(UserId));
                    PaymentInstrument paymentInstrument;

                    

                    if (userPaymentMethods.Any(x => x.IsMain))
                    {
                        paymentInstrument = userPaymentMethods.FirstOrDefault(x => x.IsMain);
                        payment.PaymentInstrumentId = paymentInstrument.Id;
                    }
                    else
                    {
                        return Json(new Response() { Success = false, Message = "No payment method" });
                    }

                    payment.Guid = new Guid();
                    payment.CreatedDate = DateTime.Now;
                    payment.CustomerId = UserId;
                    payment.Status = (int) PaymentStatus.New;
                    PaymentsRepository.Insert(payment);
                    PaymentsRepository.Save();

                    var paymentViewModel = new PaymentViewModel()
                    {
                        Amount = payment.Amount,
                        Details = payment.Description,
                        PaymentDateStr = payment.CreatedDate.ToString("g"),
                        PaymentMethod = paymentInstrument.PaypalPaymentInstrument.Account,
                        Status = PaymentStatus.New.ToString()
                    };

                    return Json(new Response { Success = true, Body = paymentViewModel});
                }
                return Json(new Response() { Success = false });
            }
            catch (Exception e)
            {
                return Json(new Response() { Success = false, Message = e.Message});
            }
            
            

        }

        public ActionResult PaymentsHistory()
        {
            var paymentMethods = PaymentInstrumentsRepository.GetAll().Where(x => x.CustomerId.Equals(UserId)).Select(s => new PaymentInstrumentViewModel()
            {
                Account = s.PaypalPaymentInstrument.Account,
                Id = s.Id,
                IsMain = s.IsMain
            }).ToList();

            var payments = PaymentsRepository.GetAll().Where(x => x.CustomerId.Equals(UserId));
            var paymentViewModelList = new List<PaymentViewModel>();
            foreach (var payment in payments)
            {
                var paymentMethod = paymentMethods.FirstOrDefault(x => x.Id == payment.PaymentInstrumentId)?.Account;
                var paymentViewModel = new PaymentViewModel()
                {
                    Amount = payment.Amount,
                    Details = payment.Description,
                    PaymentDate = payment.CreatedDate,
                    PaymentMethod = paymentMethod,
                    Status = PaymentStatus.New.ToString()
                };
                paymentViewModelList.Add(paymentViewModel);
            }

            return PartialView("_PaymentsHistory", paymentViewModelList);
        }

        public ActionResult PaymentMethods()
        {
            var pMethods = PaymentInstrumentsRepository.GetAll().Where(x => x.CustomerId.Equals(UserId)).Select(s => new PaymentInstrumentViewModel()
            {
                Account = s.PaypalPaymentInstrument.Account,
                Id = s.Id,
                IsMain = s.IsMain
            }).ToList();
            return PartialView("_PaymentMethodsPartial", pMethods);

        }

        [HttpPost]
        public JsonResult MakeMainPaymentMethod(int id)
        {
            try
            {
                var paymentMethod = PaymentInstrumentsRepository.GetById(id);
                var pMethods = PaymentInstrumentsRepository.GetAll()
                    .FirstOrDefault(x => x.CustomerId.Equals(UserId) && x.IsMain);
                if (!paymentMethod.IsMain)
                {
                    paymentMethod.IsMain = true;
                    pMethods.IsMain = false;
                    PaymentInstrumentsRepository.Save();
                    return Json(new Response() { Success = true });
                }
                return Json(new Response() { Success = false, Message = "Method already main!" });
            }
            catch
            {
                return Json(new Response() { Success = false, Message = "Error" });
            }
        }

        [HttpPost]
        public JsonResult DeletePaymentMethod(int id)
        {
            try
            {
                PaypalPaymentInstrumentRepository.Delete(id);
                PaymentInstrumentsRepository.Delete(id);
                PaypalPaymentInstrumentRepository.Save();
                PaymentInstrumentsRepository.Save();
                return Json(new Response() { Success = true });
            }
            catch
            {
                return Json(new Response() { Success = false });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreatePaymentMethod(PaymentInstrumentViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (PaymentInstrumentsRepository.GetAll().Any(x => x.CustomerId.Equals(UserId) && x.IsMain))
                {
                    model.IsMain = false;
                }
                var paymentInstrument = new PaymentInstrument()
                {
                    IsMain = model.IsMain,
                    CustomerId = UserId
                };

                PaymentInstrumentsRepository.Insert(paymentInstrument);
                PaymentInstrumentsRepository.Save();
                var paypalPaymentInstrument = new PaypalPaymentInstrument()
                {
                    Id = paymentInstrument.Id,
                    Account = model.Account
                };
                PaypalPaymentInstrumentRepository.Insert(paypalPaymentInstrument);
                PaypalPaymentInstrumentRepository.Save();

                model.Id = paypalPaymentInstrument.Id;

                return Json(new Response() {Body = model, Success = true});
            }

            return Json(new Response() {Success = false});

        }
    }
}