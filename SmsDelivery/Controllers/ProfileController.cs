﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using SmsDelivery.Model;
using SmsDelivery.Model.Interfaces;
using SmsDelivery.Models;

namespace SmsDelivery.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        public ICustomersRepository CustomersRepository { get; set; }
        public ICountryRepository CountryRepository { get; set; }

        public ProfileController(ICustomersRepository customersRepository, ICountryRepository countryRepository)
        {
            CustomersRepository = customersRepository;
            CountryRepository = countryRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        // GET: Profile/Edit/5
        public ActionResult Edit()
        {
            Customer customer = CustomersRepository.GetById(User.Identity.GetUserId());
            if (customer == null)
            {
                return HttpNotFound();
            }
            ViewBag.Countries = new SelectList(CountryRepository.GetAll().ToList(), "Id", "Name", customer.CountryId);
            ViewBag.TimeZones = new SelectList(TimeZoneInfo.GetSystemTimeZones(), "Id", "DisplayName");
            return PartialView("_EditPartial", customer);
        }

        // POST: Profile/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit([Bind(Include = "Id,Name,Surname,PhoneNumber,CompanyName,BillingAddress,TimeZone,CountryId,UserName,Email")] Customer model)
        {
            if (ModelState.IsValid)
            {
                Customer customer = CustomersRepository.GetById(User.Identity.GetUserId());
                customer.BillingAddress = model.BillingAddress;
                customer.RegistrationNumber = model.RegistrationNumber;
                customer.VatNumber = model.VatNumber;
                customer.City = model.City;
                customer.CompanyName = model.City;
                customer.CountryId = model.CountryId;
                customer.Email = model.Email;
                customer.PhoneNumber = model.PhoneNumber;
                customer.Name = model.Name;
                customer.Surname = model.Surname;
                customer.UserName = model.Email;
                customer.TimeZone = model.TimeZone;
                //CustomersRepository.Update(customer);
                CustomersRepository.Save();
                return Json(new Response { Success = true });
            }
            return Json(new Response(){Success = false});
        }
    }
}
