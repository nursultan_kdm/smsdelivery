﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using SmsDelivery.Model;
using SmsDelivery.Model.Interfaces;
using SmsDelivery.Models;
using SmsDelivery.Models.FilteringAndSorting;

namespace SmsDelivery.Controllers
{
    [Authorize]
    public class SubscriberGroupsController : Controller
    {
        //private SmsDeliveryDbEntities db = new SmsDeliveryDbEntities();
        public ISubscriberGroupRepository SubscriberGroupRepository { get; set; }

        public SubscriberGroupsController(ISubscriberGroupRepository subscriberGroupRepository)
        {
            SubscriberGroupRepository = subscriberGroupRepository;
        }

        // GET: SubscriberGroups
        public ActionResult Index(SubscriberGroupsFilteredModel filteredModel, int? page)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            if (filteredModel == null)
            {
                filteredModel = new SubscriberGroupsFilteredModel();
            }
            if (filteredModel.PageSize <= 0)
            {
                filteredModel.PageSize = 10;
            }

            var subscribers = SubscriberGroupRepository.GetAll();
            filteredModel.ProcessData(subscribers, "ID", true, pageNumber);

            return View(filteredModel);
        }

        // GET: SubscriberGroups/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubscriberGroup subscriberGroup = SubscriberGroupRepository.GetById(id);
            if (subscriberGroup == null)
            {
                return HttpNotFound();
            }
            return View(subscriberGroup);
        }

        // GET: SubscriberGroups/Create
        public ActionResult CreateModal()
        {
            return PartialView("_CreateModal");
        }

        // POST: SubscriberGroups/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create([Bind(Include = "Id,Name,Description,CustomerId,IsMain")] SubscriberGroup subscriberGroup)
        {
            if (ModelState.IsValid)
            {
                subscriberGroup.CustomerId = User.Identity.GetUserId();
                SubscriberGroupRepository.Insert(subscriberGroup);
                SubscriberGroupRepository.Save();
                return Json(new Response { Success = true, Message = subscriberGroup.Id.ToString()});
            }

            return Json(new Response { Success = false });
        }

        // GET: SubscriberGroups/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubscriberGroup subscriberGroup = SubscriberGroupRepository.GetById(id);
            if (subscriberGroup == null)
            {
                return HttpNotFound();
            }
            return View(subscriberGroup);
        }

        // POST: SubscriberGroups/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,CustomerId,IsMain")] SubscriberGroup subscriberGroup)
        {
            if (ModelState.IsValid)
            {
                SubscriberGroupRepository.Update(subscriberGroup);
                SubscriberGroupRepository.Save();
                return RedirectToAction("Index");
            }
            return View(subscriberGroup);
        }

        // GET: SubscriberGroups/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubscriberGroup subscriberGroup = SubscriberGroupRepository.GetById(id);
            if (subscriberGroup == null)
            {
                return HttpNotFound();
            }
            return View(subscriberGroup);
        }

        // POST: SubscriberGroups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SubscriberGroup subscriberGroup = SubscriberGroupRepository.GetById(id);
            SubscriberGroupRepository.Delete(subscriberGroup);
            SubscriberGroupRepository.Save();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult DeleteSelected(int[] ids)
        {
            if (ids.Length > 0)
            {
                foreach (var id in ids)
                {
                    SubscriberGroupRepository.Delete(id);
                }
                SubscriberGroupRepository.Save();

                return Json(new Response() { Success = true });
            }
            return Json(new Response() { Success = false });
        }
    }
}
