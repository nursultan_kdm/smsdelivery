﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using SmsDelivery.Filters;
using SmsDelivery.Model;
using SmsDelivery.Model.Interfaces;
using SmsDelivery.Models;
using SmsDelivery.Models.FilteringAndSorting;

namespace SmsDelivery.Controllers
{
    [Authorize]
    public class SubscribersController : Controller
    {
        public ISubscriberRepository SubscriberRepository { get; set; }
        public ICountryRepository CountryRepository { get; set; }
        public ISubscriberGroupRepository SubscriberGroupRepository { get; set; }

        public SubscribersController(ISubscriberRepository subscriberRepository, ICountryRepository countryRepository, ISubscriberGroupRepository subscriberGroupRepository)
        {
            SubscriberRepository = subscriberRepository;
            CountryRepository = countryRepository;
            SubscriberGroupRepository = subscriberGroupRepository;
        }

        // GET: Subscribers
        public ActionResult Index(SubscribersFilteredModel filteredModel, int? page)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            if (filteredModel == null)
            {
                filteredModel = new SubscribersFilteredModel();
            }
            if (filteredModel.PageSize <= 0)
            {
                filteredModel.PageSize = 10;
            }

            var subscribers = SubscriberRepository.GetAll();

            filteredModel.ProcessData(subscribers, "ID", true, pageNumber);
            return View(filteredModel);
        }

        // GET: Subscribers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subscriber subscriber = SubscriberRepository.GetById(id);
            if (subscriber == null)
            {
                return HttpNotFound();
            }
            return View(subscriber);
        }

        // GET: Subscribers/Create
        public ActionResult GetCreatePartial()
        {
            ViewBag.CountryId = new SelectList(CountryRepository.GetAll().ToList(), "Id", "Name");
            ViewBag.SubscriberGroupId = new SelectList(SubscriberGroupRepository.GetAll().ToList(), "Id", "Name");
            return PartialView("_CreateModal");
        }

        // POST: Subscribers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create([Bind(Include = "Id,Name,Surname,Phone,Email,Address,City,CountryId,SubscriberGroupId")] Subscriber subscriber)
        {
            if (ModelState.IsValid)
            {
                subscriber.CreatedDate = DateTime.Now;
                SubscriberRepository.Insert(subscriber);
                SubscriberRepository.Save();
                return Json(new Response{ Success = true, Message = subscriber.Id.ToString()}, JsonRequestBehavior.AllowGet);
            }
            ViewBag.CountryId = new SelectList(CountryRepository.GetAll().ToList(), "Id", "Name");
            ViewBag.SubscriberGroupId = new SelectList(SubscriberGroupRepository.GetAll().ToList(), "Id", "Name");
            return Json(new Response {Success = false, Errors = ModelState.Keys.ToList()});
        }

        // GET: Subscribers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subscriber subscriber = SubscriberRepository.GetById(id);
            if (subscriber == null)
            {
                return HttpNotFound();
            }
            ViewBag.CountryId = new SelectList(CountryRepository.GetAll().ToList(), "Id", "Name");
            ViewBag.SubscriberGroupId = new SelectList(SubscriberGroupRepository.GetAll().ToList(), "Id", "Name");
            return View(subscriber);
        }

        // POST: Subscribers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Surname,Phone,Email,Address,City,CountryId,CreatedDate")] Subscriber subscriber)
        {
            if (ModelState.IsValid)
            {
                SubscriberRepository.Update(subscriber);
                SubscriberRepository.Save();
                return RedirectToAction("Index");
            }
            ViewBag.CountryId = new SelectList(CountryRepository.GetAll().ToList(), "Id", "Name");
            ViewBag.SubscriberGroupId = new SelectList(SubscriberGroupRepository.GetAll().ToList(), "Id", "Name");
            return View(subscriber);
        }

        // GET: Subscribers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subscriber subscriber = SubscriberRepository.GetById(id);
            if (subscriber == null)
            {
                return HttpNotFound();
            }
            return View(subscriber);
        }

        // POST: Subscribers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SubscriberRepository.Delete(id);
            SubscriberRepository.Save();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult DeleteSelected(int[] ids)
        {
            if (ids.Length > 0)
            {
                foreach (var id in ids)
                {
                    SubscriberRepository.Delete(id);
                }
                SubscriberRepository.Save();

                return Json(new Response() {Success = true});
            }
            return Json(new Response() {Success = false});
        }
    }
}
