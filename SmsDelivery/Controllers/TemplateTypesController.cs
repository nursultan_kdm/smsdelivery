﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using SmsDelivery.Model;
using SmsDelivery.Model.Interfaces;
using SmsDelivery.Models;
using SmsDelivery.Models.FilteringAndSorting;

namespace SmsDelivery.Controllers
{
    public class TemplateTypesController : Controller
    {
        public string UserId => User.Identity.GetUserId();
        public ITemplateTypesRepository TemplateTypesRepository { get; set; }

        public TemplateTypesController(ITemplateTypesRepository templateTypesRepository)
        {
            TemplateTypesRepository = templateTypesRepository;
        }

        // GET: Templates
        public ActionResult Index(TemplateTypesFilteredModel templateTypesFilteredModel, int? page)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            if (templateTypesFilteredModel == null)
            {
                templateTypesFilteredModel = new TemplateTypesFilteredModel();
            }

            if (templateTypesFilteredModel.PageSize <= 0)
            {
                templateTypesFilteredModel.PageSize = 10;
            }

            var templateTypes = TemplateTypesRepository.GetAll();

            templateTypesFilteredModel.ProcessData(templateTypes, "id", true, pageNumber);

            return View(templateTypesFilteredModel);
        }

        public ActionResult CreateModal()
        {
            //ViewBag.TemplateTypes = new SelectList(TemplateTypesRepository.GetAll().ToList(), "Id", "Type");
            return PartialView("_CreateModal");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create(TemplateType templateType)
        {
            if (ModelState.IsValid)
            {
                //templateType.CustomerId = UserId;
                TemplateTypesRepository.Insert(templateType);
                TemplateTypesRepository.Save();

                return Json(new Response { Success = true, Body = templateType });
            }

            return Json(new Response() { Success = false });
        }

        public ActionResult EditModal()
        {
            ViewBag.TemplateTypes = new SelectList(TemplateTypesRepository.GetAll().ToList(), "Id", "Type");
            return PartialView("_EditModal");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(Template template)
        {
            if (ModelState.IsValid)
            {
                //TemplatesRepository.Update(template);
                //TemplatesRepository.Save();

                return Json(new Response() { Success = true });
            }
            return Json(new Response() { Success = false });
        }

        [HttpPost]
        public JsonResult DeleteSelected(int[] ids)
        {
            if (ids.Length > 0)
            {
                foreach (var id in ids)
                {
                    //TemplatesRepository.Delete(id);
                }
                //TemplatesRepository.Save();

                return Json(new Response() { Success = true });
            }
            return Json(new Response() { Success = false });
        }
    }
}