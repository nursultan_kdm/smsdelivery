﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using SmsDelivery.Model;
using SmsDelivery.Model.Interfaces;
using SmsDelivery.Models;
using SmsDelivery.Models.FilteringAndSorting;

namespace SmsDelivery.Controllers
{
    public class TemplatesController : Controller
    {
        public string UserId => User.Identity.GetUserId();
        public ITemplatesRepository TemplatesRepository { get; set; }
        public ITemplateTypesRepository TemplateTypesRepository { get; set; }

        public TemplatesController(ITemplatesRepository templatesRepository, ITemplateTypesRepository templateTypesRepository)
        {
            TemplatesRepository = templatesRepository;
            TemplateTypesRepository = templateTypesRepository;
        }

        // GET: Templates
        public ActionResult Index(TemplatesFilteredModel templatesFilteredModel, int? page)
        {
            var pageNumber = Request.HttpMethod == "GET" ? page ?? 1 : 1;

            if (templatesFilteredModel == null)
            {
                templatesFilteredModel = new TemplatesFilteredModel();
            }

            if (templatesFilteredModel.PageSize <= 0)
            {
                templatesFilteredModel.PageSize = 10;
            }

            var templates = TemplatesRepository.GetAll();

            templatesFilteredModel.ProcessData(templates, "id", true, pageNumber);

            return View(templatesFilteredModel);
        }

        public ActionResult CreateModal()
        {
            ViewBag.TemplateTypes = new SelectList(TemplateTypesRepository.GetAll().ToList(), "Id", "Type");
            return PartialView("_CreateModal");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create(Template template)
        {
            if (ModelState.IsValid)
            {
                template.CustomerId = UserId;
                TemplatesRepository.Insert(template);
                TemplatesRepository.Save();

                return Json(new Response {Success = true, Body = template});
            }

            return Json(new Response() {Success = false});
        }

        public ActionResult EditModal()
        {
            ViewBag.TemplateTypes = new SelectList(TemplateTypesRepository.GetAll().ToList(), "Id", "Type");
            return PartialView("_EditModal");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(Template template)
        {
            if (ModelState.IsValid)
            {
                TemplatesRepository.Update(template);
                TemplatesRepository.Save();

                return Json(new Response() {Success = true});
            }
            return Json(new Response() {Success = false});
        }

        [HttpPost]
        public JsonResult DeleteSelected(int[] ids)
        {
            if (ids.Length > 0)
            {
                foreach (var id in ids)
                {
                    TemplatesRepository.Delete(id);
                }
                TemplatesRepository.Save();

                return Json(new Response() { Success = true });
            }
            return Json(new Response() { Success = false });
        }
    }
}