﻿using System.Web.Mvc;
using Ninject;

namespace SmsDelivery.DI
{
    public class NinjectBootstraper
    {
        public static IKernel Kernel { get; private set; }

        public static void Initialize()
        {
            Kernel = new StandardKernel(new NinjectModuleExt());
            DependencyResolver.SetResolver(new NinjectDependencyResolver(Kernel));
        }
    }
}