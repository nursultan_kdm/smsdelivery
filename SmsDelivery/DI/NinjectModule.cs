﻿using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Ninject.Modules;
using SmsDelivery.Model;
using SmsDelivery.Model.Interfaces;
using SmsDelivery.Model.Repository;
using SmsDelivery.Models.CustomIdentityImplementation;

namespace SmsDelivery.DI
{
    public class NinjectModuleExt : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserStore<Customer>>().To<UserStore>();

            Bind<HttpContextBase>().ToMethod(ctx => new HttpContextWrapper(HttpContext.Current)).InTransientScope();

            Bind<ApplicationSignInManager>().ToSelf();

            Bind<ApplicationUserManager>().ToSelf();
            Bind<IAuthenticationManager>().ToMethod(x => HttpContext.Current.GetOwinContext().Authentication);

            //Bind<ICustomerClaimRepository>().To<CustomerClaimRepository>();
            Bind<ICustomersRepository>().To<CustomersRepository>();
            Bind<ISubscriberRepository>().To<SubscriberRepository>();
            Bind<ISubscriberGroupRepository>().To<SubscriberGroupRepository>();
            Bind<ICountryRepository>().To<CountryRepository>();
            Bind<IPaymentInstrumentsRepository>().To<PaymentInstrumentsRepository>();
            Bind<IPaymentsRepository>().To<PaymentsRepository>();
            Bind<IPaypalPaymentInstrumentRepository>().To<PaypalPaymentInstrumentRepository>();
            Bind<ITemplateTypesRepository>().To<TemplateTypesRepository>();
            Bind<ITemplatesRepository>().To<TemplatesRepository>();
            Bind<ICampaignRepository>().To<CampaignRepository>();
            Bind<ISavedSenderNameRepository>().To<SavedSenderNameRepository>();

        }
    }
}