﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using SmsDelivery.Model;

namespace SmsDelivery.Models.CustomIdentityImplementation
{
    public class ClaimsIdentityFactory : ClaimsIdentityFactory<Customer, string>
    {
        public override async System.Threading.Tasks.Task<ClaimsIdentity> CreateAsync(UserManager<Customer, string> manager, Customer user, string authenticationType)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.Email, user.Email)
            };
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, authenticationType);
            return claimsIdentity;
        }
    }
}