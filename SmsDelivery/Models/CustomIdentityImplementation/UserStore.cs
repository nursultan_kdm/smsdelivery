﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using SmsDelivery.Model;
using SmsDelivery.Model.Interfaces;
using SmsDelivery.Model.Repository;

namespace SmsDelivery.Models.CustomIdentityImplementation
{
    public class UserStore : IUserStore<Customer>,
        IUserRoleStore<Customer>,
        IUserPasswordStore<Customer>,
        IUserEmailStore<Customer>,
        IUserSecurityStampStore<Customer>,
        IUserLockoutStore<Customer, string>,
        IUserTwoFactorStore<Customer, string>

    {
        private ICustomersRepository CustomerRepository { get; set; }
        public UserStore()
        {
            CustomerRepository = new CustomersRepository();
        }
        public void Dispose()
        {
            if (CustomerRepository == null)
            {
                return;
            }
            if (CustomerRepository != null)
            {
                CustomerRepository.Save();
                CustomerRepository.Dispose();
            }
        }

        public Task CreateAsync(Customer user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }


            user.Id = Guid.NewGuid().ToString();
            CustomerRepository.Insert(user);
            CustomerRepository.Save();

            return Task.FromResult<object>(null);
        }

        public Task UpdateAsync(Customer user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }


            var customer = CustomerRepository.GetAll().FirstOrDefault(x => x.Id.Equals(user.Id));
            if (customer == null)
            {
                throw new ArgumentNullException("user");
            }
            CustomerRepository.Update(customer);
            CustomerRepository.Save();

            return Task.FromResult<object>(null);
        }

        public Task DeleteAsync(Customer user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            CustomerRepository.Delete(user.Id);

            return Task.FromResult<object>(null);
        }

        public Task<Customer> FindByIdAsync(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentException("Null or empty argument: userId");
            }

            var result = CustomerRepository.GetAll().FirstOrDefault(x => x.Id.Equals(userId));
            if (result != null)
            {
                return Task.FromResult(result);
            }

            return Task.FromResult<Customer>(null);
        }

        public Task<Customer> FindByNameAsync(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                throw new ArgumentException("Null or empty argument: userName");
            }

            var result = CustomerRepository.GetAll().FirstOrDefault(x => x.UserName.Equals(userName));
            if (result != null)
            {
                return Task.FromResult(result);
            }

            return Task.FromResult<Customer>(null);
        }

        public Task AddToRoleAsync(Customer user, string roleName)
        {
            //if (!user.LegalBodyUserRoles.Any(x => x.LegalBodyRole.Name.Contains(roleName)))
            //{
            //    var role = LegalBodyRoleRepository.All.FirstOrDefault(x => x.Name.Equals(roleName));
            //    var userRole = new LegalBodyUserRole();
            //    userRole.LegalBodyRoleId = role.Id;
            //    userRole.LegalBodyUserId = user.Id;
            //    LegalBodyUserRoleRepository.InsertOrUpdate(userRole);

            //    return Task.FromResult(userRole);
            //}

            return Task.FromResult<object>(null);
        }

        public Task RemoveFromRoleAsync(Customer user, string roleName)
        {

            //var role = LegalBodyRoleRepository.All.FirstOrDefault(x => x.Name.Equals(roleName));
            //var userRole = LegalBodyUserRoleRepository.Find(role.Id);
            //LegalBodyUserRoleRepository.Delete(userRole.Id);
            //LegalBodyUserRoleRepository.Save();

            return Task.FromResult<object>(null);
        }

        public Task<IList<string>> GetRolesAsync(Customer user)
        {
            //return Task.FromResult((IList<string>)user.LegalBodyUserRoles.Select(x => x.LegalBodyRole.Name).ToList());
            return Task.FromResult<IList<string>>(null);
        }

        public Task<bool> IsInRoleAsync(Customer user, string roleName)
        {

            //var role = user.LegalBodyUserRoles.FirstOrDefault(x => x.LegalBodyRole.Name.Equals(roleName));
            //if (role != null)
            //{
            //    return Task.FromResult(true);
            //}
            //else
            //{
            //    return Task.FromResult(false);
            //}
            return Task.FromResult(false);

        }

        public Task SetPasswordHashAsync(Customer user, string passwordHash)
        {
            user.PasswordHash = passwordHash;
            return Task.FromResult(0);
        }

        public Task<string> GetPasswordHashAsync(Customer user)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(Customer user)
        {
            return Task.FromResult(string.IsNullOrEmpty(user.PasswordHash));
        }

        public Task SetEmailAsync(Customer user, string email)
        {
            user.Email = email;
            return Task.FromResult(0);
        }

        public Task<string> GetEmailAsync(Customer user)
        {
            return Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(Customer user)
        {
            return Task.FromResult(true);
        }

        public Task SetEmailConfirmedAsync(Customer user, bool confirmed)
        {
            return Task.FromResult(true);
        }

        public Task<Customer> FindByEmailAsync(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                throw new ArgumentException("Null or empty argument: userName");
            }

            var result = CustomerRepository.GetAll().FirstOrDefault(x => x.Email.Equals(email));
            if (result != null)
            {
                return Task.FromResult(result);
            }

            return Task.FromResult<Customer>(null);
        }

        public Task SetSecurityStampAsync(Customer user, string stamp)
        {
            user.SecurityStamp = stamp;
            return Task.FromResult(0);
        }

        public Task<string> GetSecurityStampAsync(Customer user)
        {
            return Task.FromResult(user.SecurityStamp);
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(Customer user)
        {
            throw new NotImplementedException();
        }

        public Task SetLockoutEndDateAsync(Customer user, DateTimeOffset lockoutEnd)
        {
            throw new NotImplementedException();
        }

        public Task<int> IncrementAccessFailedCountAsync(Customer user)
        {
            throw new NotImplementedException();
        }

        public Task ResetAccessFailedCountAsync(Customer user)
        {
            throw new NotImplementedException();
        }

        public Task<int> GetAccessFailedCountAsync(Customer user)
        {
            return Task.FromResult(0);
        }

        public Task<bool> GetLockoutEnabledAsync(Customer user)
        {
            return Task.Factory.StartNew<bool>(() => false);
        }

        public Task SetLockoutEnabledAsync(Customer user, bool enabled)
        {
            return Task.Factory.StartNew<bool>(() => false);
        }

        public Task SetTwoFactorEnabledAsync(Customer user, bool enabled)
        {
            throw new NotImplementedException();
        }

        public Task<bool> GetTwoFactorEnabledAsync(Customer user)
        {
            return Task.FromResult(false);
        }
    }
}