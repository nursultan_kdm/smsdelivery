﻿using System.Linq;
using System.Web.WebPages;
using PagedList;
using SmsDelivery.Model;
using SmsDelivery.Model.Enums;

namespace SmsDelivery.Models.FilteringAndSorting
{
    public class CampaignsFilteredModel
    {
        public IPagedList<Campaing> Campaings { get; set; }

        public int PageSize { get; set; }
        public bool EmailCampaigns { get; set; }
        public bool SmsCampaigns { get; set; }
        public void ProcessData(IQueryable<Campaing> campaings, string sortColumn, bool dir, int pageNumber)
        {
            Campaings = SortData(FilterData(campaings), sortColumn, dir).ToPagedList(pageNumber, PageSize);
        }

        public IQueryable<Campaing> FilterData(IQueryable<Campaing> campaings)
        {
            if (EmailCampaigns) campaings = campaings.Where(x => x.CampaignTypeId == (int) CampaignTypes.Email);
            if (SmsCampaigns) campaings = campaings.Where(x => x.CampaignTypeId == (int) CampaignTypes.Sms);
            return campaings;
        }

        public IOrderedQueryable<Campaing> SortData(IQueryable<Campaing> groups, string sortColumn, bool dir)
        {
            //if (sortColumn == "ID") return groups.OrderByWithDirection(a => a.Id, dir);
            return groups.OrderBy(a => a.Id);
        }
    }
}