﻿namespace SmsDelivery.Models.FilteringAndSorting
{
    public enum OrderDirection
    {
        Ascending,
        Descending
    }
}