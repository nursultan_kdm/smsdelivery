﻿using System.Linq;
using System.Web.WebPages;
using PagedList;
using SmsDelivery.Model;

namespace SmsDelivery.Models.FilteringAndSorting
{
    public class SubscriberGroupsFilteredModel
    {
        public IPagedList<SubscriberGroup> SubscriberGroups { get; set; }
        public int PageSize { get; set; }
        public string SearchText { get; set; }
        public void ProcessData(IQueryable<SubscriberGroup> subscriberGroups, string sortColumn, bool dir, int pageNumber)
        {
            SubscriberGroups = SortData(FilterData(subscriberGroups), sortColumn, dir).ToPagedList(pageNumber, PageSize);
        }

        public IQueryable<SubscriberGroup> FilterData(IQueryable<SubscriberGroup> subscriberGroups)
        {
            if (!SearchText.IsEmpty())
            {
                subscriberGroups = subscriberGroups.Where(x => x.Name.Contains(SearchText));
            }
            return subscriberGroups;
        }

        public IOrderedQueryable<SubscriberGroup> SortData(IQueryable<SubscriberGroup> groups, string sortColumn, bool dir)
        {
            //if (sortColumn == "ID") return groups.OrderByWithDirection(a => a.Id, dir);
            return groups.OrderBy(a => a.Id);
        }
    }
}