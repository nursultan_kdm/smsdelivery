﻿using System.Linq;
using System.Web.WebPages;
using PagedList;
using SmsDelivery.Model;

namespace SmsDelivery.Models.FilteringAndSorting
{
    public class SubscribersFilteredModel
    {
        public IPagedList<Subscriber> Subscribers { get; set; }

        public int PageSize { get; set; }
       
        public string SearchText { get; set; } 
        public void ProcessData(IQueryable<Subscriber> subscribers, string sortColumn, bool dir, int pageNumber)
        {
            Subscribers = SortData(FilterData(subscribers), sortColumn, dir).ToPagedList(pageNumber, PageSize);
        }

        public IQueryable<Subscriber> FilterData(IQueryable<Subscriber> subscribers)
        {
            if (!SearchText.IsEmpty())
            {
                subscribers = subscribers.Where(x => x.Name.Contains(SearchText) || x.Surname.Contains(SearchText));
            }
            return subscribers;
        }

        public IOrderedQueryable<Subscriber> SortData(IQueryable<Subscriber> groups, string sortColumn, bool dir)
        {
            //if (sortColumn == "ID") return groups.OrderByWithDirection(a => a.Id, dir);
            return groups.OrderBy(a => a.Id);
        }
    }
}