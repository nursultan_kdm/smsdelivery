﻿using System.Linq;
using PagedList;
using SmsDelivery.Model;

namespace SmsDelivery.Models.FilteringAndSorting
{
    public class TemplateTypesFilteredModel
    {
        public IPagedList<TemplateType> TemplateTypes { get; set; }
        public int PageSize { get; set; }

        public void ProcessData(IQueryable<TemplateType> templateTypes, string sortColumn, bool dir, int pageNumber)
        {
            TemplateTypes = SortData(FilterData(templateTypes), sortColumn, dir).ToPagedList(pageNumber, PageSize);
        }

        public IQueryable<TemplateType> FilterData(IQueryable<TemplateType> templateTypes)
        {
            return templateTypes;
        }

        public IOrderedQueryable<TemplateType> SortData(IQueryable<TemplateType> groups, string sortColumn, bool dir)
        {
            //if (sortColumn == "ID") return groups.OrderByWithDirection(a => a.Id, dir);
            return groups.OrderBy(a => a.Id);
        }
    }
}