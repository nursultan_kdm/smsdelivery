﻿using System.Linq;
using PagedList;
using SmsDelivery.Model;

namespace SmsDelivery.Models.FilteringAndSorting
{
    public class TemplatesFilteredModel
    {
        public IPagedList<Template> Templates { get; set; }
        public int PageSize { get; set; }

        public void ProcessData(IQueryable<Template> templates, string sortColumn, bool dir, int pageNumber)
        {
            Templates = SortData(FilterData(templates), sortColumn, dir).ToPagedList(pageNumber, PageSize);
        }

        public IQueryable<Template> FilterData(IQueryable<Template> templates)
        {
            return templates;
        }

        public IOrderedQueryable<Template> SortData(IQueryable<Template> groups, string sortColumn, bool dir)
        {
            //if (sortColumn == "ID") return groups.OrderByWithDirection(a => a.Id, dir);
            return groups.OrderBy(a => a.Id);
        }
    }
}