﻿using System.Collections.Generic;

namespace SmsDelivery.Models
{
    public class Response
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public List<string> Errors { get; set; }
        public object Body { get; set; }
    }
}