﻿jQuery(function() {
    $("#addAnother").click(function () {
        $.get('/Campaigns/SubscriberGroupEntryRow', function (template) {
            $("#subscriberGroups").append(template);
        });
    });

    $('.switch-plain').bootstrapSwitch({
        onColor: '#03A9F4',
        onText: '',
        offText: ''
    });

    function OnSelectedFunctionName(event) {
        //The default datum provided by this plugin has an id and name attribute
        var obj$ = $(event.target);
        var datum = {
            id: event.target.value,
            value: event.target.value
        };
        //Handle the selection value here//
    }
});