﻿jQuery(function() {
    $("#paymentMethodCreateModalForm").on("submit", function (event) {
        event.preventDefault();
        var url = $(this).data('url');
        var formData = objectifyForm($(this).serializeArray());
        $.ajax({
            type: 'POST',
            url: url,
            data: $(this).serialize(),
            dataType: "json",
            success: function (result) {
                if (result.Success) {
                    if ($('#paymentMethodsNoData').length > 0) {
                        $('#paymentMethodsNoData').remove();
                    }
                    var view = addPaymentMethodView(result.Body.IsMain, result.Body.Account, result.Body.Id);
                    $(".payment-methods-container").children('.row').append(view);
                    $("#paymentMethodCreateModal").modal('hide');

                } else {
                    if (result.Errors.length > 0) {
                        for (var i = 0; i < result.Errors.length; i++) {
                            $("#validationErrors").find("ul").append("<li>" + result.Errors[i] + "</li>");
                        }
                        $("#validationErrors").show();
                    }
                }
            }
        });

    });

    $("#paymentCreateForm").on("submit", function (event) {
        event.preventDefault();
        var url = $(this).data('url');
        var formData = objectifyForm($(this).serializeArray());
        $.ajax({
            type: 'POST',
            url: url,
            data: $(this).serialize(),
            dataType: "json",
            success: function (result) {
                if (result.Success) {
                    if ($('#noPaymentsDataRow').length > 0) {
                        $('#noPaymentsDataRow').remove();
                    }
                    var view = addPaymentRow(result.Body);
                    $(".payments-history-container").find('tbody').append(view);
                    showNotification("success", "Payment successfully added!");
                } else {
                    //if (result.Errors.length > 0) {
                    //    for (var i = 0; i < result.Errors.length; i++) {
                    //        $("#validationErrors").find("ul").append("<li>" + result.Errors[i] + "</li>");
                    //    }
                    //    $("#validationErrors").show();
                    //}
                    showNotification("danger", "Error: " + result.Message);
                }
            }
        });

    });

    $('.delete-paymentmethod').click(function() {
        var id = $(this).data('id');
        var url = $('#paymentMethodDeleteUrl').data('url');
        var thisElement = $(this);
        $.ajax({
            type: 'POST',
            url: url,
            data: { id },
            success: function(result) {
                if (result.Success) {
                    var parent = thisElement.parents()[4];
                    parent.remove();
                    if ($('.payment-method-div').length === 0) {
                        $(".payment-methods-container").children().append('<div class="text-center" id="paymentMethodsNoData"><p>No data</p></div>');
                    }
                    showNotification("success", "Payment method deleted!");
                } else {
                    showNotification("danger", "Error!");
                }
            }
        });
    });

    $('body').on('click', '.payment-method-primary', function () {
        var id = $(this).data('id');
        var url = $('#paymentMethodMakeMainUrl').data('url');
        var isMain = $(this).data('ismain');

        if (isMain === false) {
            $.ajax({
                type: 'POST',
                url: url,
                data: { id },
                success: function (result) {
                    if (result.Success) {
                        $('.payment-method-primary').each(function(i, obj) {
                            if ($(obj).data('id') === id) {
                                $(obj).data('ismain', true);
                                $(obj).text('Primary');
                            } else {
                                $(obj).data('ismain', false);
                                $(obj).text('Make Primary');
                            }
                        });
                        showNotification("success", "Changing primary method success!");
                    } else {
                        showNotification("danger", "Error!");
                    }
                }
            });
        } else {
            showNotification("info", "Method already primary!");
        }
    });

    function objectifyForm(formArray) {//serialize data function

        var returnArray = {};
        for (var i = 0; i < formArray.length; i++) {

            //if (formArray[i]['name'] !== '__RequestVerificationToken') {
            returnArray[formArray[i]['name']] = formArray[i]['value'];
            //}

        }
        return returnArray;
    }

    function showNotification(type, message) {
        $.notify({
            // options
            icon: 'glyphicon glyphicon-warning-sign',
            message: message
        }, {
            // settings
            type: type,
            placement: {
                from: "top",
                align: "center"
            },
            offset: 20,
            delay: 3000,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            }
        });
    }

    function addPaymentMethodView(main, account, id) {

        var methodMainOrNot;
        if (main === true) {
            methodMainOrNot = '<a href="#" data-id="' + id + '" data-ismain="true" class="payment-method-primary text-uppercase">Primary</a>';
        } else {
            methodMainOrNot = '<a href="#" data-id="' + id + '" data-ismain="false" class="payment-method-primary text-uppercase">Make Primary</a>';
        }

        return '<div class="col-lg-4 col-sm-6 payment-method-div">' +
                '<div class="card">' +
                    '<div class="card-content">' +
                        '<div class="row">' +
                            '<div class="col-md-5">' +
                                '<div class="numbers">' +
                                    '<p class="text-center">Account:</p>' +
                                    '<p><b>' + account + '</b></p>' +
                                '</div>' +
                            '</div>' +
                            '<div class="col-md-5 text-center">' +
                                methodMainOrNot +
                            '</div>' +
                            '<div class="col-md-2 text-center">' +
                                '<a href="#" data-id="' + id + '" class="delete-paymentmethod text-uppercase"><div class="text-center">' +
                                    '<i class="ti-close"></i>' +
                                '</div></a>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>';
    }

    function addPaymentRow(payment) {
        return '<tr>' + 
            '<td>' + payment.PaymentDateStr + '</td>' + 
            '<td>' + payment.Amount + '</td>' +
            '<td>' + payment.PaymentMethod + '</td>' + 
            '<td>' + payment.Details + '</td>' + 
            '<td>' + payment.Status + '</td>' +
        + '</tr>';
    }
})