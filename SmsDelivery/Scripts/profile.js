﻿jQuery(function() {
    $("#profileEditForm").on("submit", function (event) {
        event.preventDefault();
        var url = $(this).data('url');
        var formData = objectifyForm($(this).serializeArray());
        $.ajax({
            type: 'POST',
            url: url,
            data: $(this).serialize(),
            dataType: "json",
            success: function (result) {
                if (result.Success) {
                    showNotification('success', 'Profile edited success!');

                } else {
                    showNotification('danger', 'Error!');
                }
            }
        });

    });

    $("#changePasswordForm").on("submit", function (event) {
        event.preventDefault();
        var url = $(this).data('url');
        var formData = objectifyForm($(this).serializeArray());
        $.ajax({
            type: 'POST',
            url: url,
            data: $(this).serialize(),
            dataType: "json",
            success: function (result) {
                if (result.Success) {
                    showNotification('success', 'Password successfully changed!');

                } else {
                    showNotification('danger', 'Error!');
                }
            }
        });

    });

    function objectifyForm(formArray) {//serialize data function

        var returnArray = {};
        for (var i = 0; i < formArray.length; i++) {

            //if (formArray[i]['name'] !== '__RequestVerificationToken') {
            returnArray[formArray[i]['name']] = formArray[i]['value'];
            //}

        }
        return returnArray;
    }

    function showNotification(type, message) {
        $.notify({
            // options
            icon: 'glyphicon glyphicon-warning-sign',
            message: message
        }, {
            // settings
            type: type,
            placement: {
                from: "top",
                align: "center"
            },
            offset: 20,
            delay: 3000,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            }
        });
    }
});