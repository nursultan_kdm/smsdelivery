﻿jQuery(function() {
    $("#subscriberCreateModalForm").on("submit", function (event) {
        event.preventDefault();
        var url = $(this).data('url');
        var formData = objectifyForm($(this).serializeArray());
        $.ajax({
            type: 'POST',
            url: url,
            data: $(this).serialize(),
            dataType: "json",
            success: function(result) {
                if (result.Success) {
                    var subscriberGroupName =
                        $("#SubscriberGroupId option[value=" + formData.SubscriberGroupId + "]").text();
                    var newRow = "<tr>" +
                        "<td><input data-id=" + result.Message + " id=\"ToDelete\" name=\"ToDelete\" type=\"checkbox\" value=\"true\"></td>" +
                        "<td>" + formData.Phone + "</td>" +
                        "<td>" + formData.Name + " " + formData.Surname + "</td>" +
                        "<td>" + formData.Email + "</td>" +
                        "<td>" + subscriberGroupName + "</td>";
                    if ($('#noDataRow').length) {
                        $('#noDataRow').parent().remove();
                    }
                    $("tbody").append(newRow);

                    $("#subscriberCreateModal").modal('hide');
                    
                } else {
                    if (result.Errors.length > 0) {
                        for (var i = 0; i < result.Errors.length; i++) {
                            $("#validationErrors").find("ul").append("<li>" + result.Errors[i] + "</li>");
                        }
                        $("#validationErrors").show();
                    }
                }
            }
        });

    });

    $('.delete-checked-subscribers').click(function () {
        var url = $(this).data('url');
        var subscriberIdsToDelete = [];
        var trs = $('table > tbody > tr');
        $('table > tbody > tr #ToDelete:checked').each(function () {
            subscriberIdsToDelete.push($(this).data('id'));
        });
        bootbox.confirm({
            title: "Subscribers delete",
            message: "Are you sure?",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                if (result) {
                    if (subscriberIdsToDelete.length > 0) {
                        $.post(url, { ids: subscriberIdsToDelete }, function (result) {
                            if (result.Success) {
                                for (var i = 0; i < subscriberIdsToDelete.length; i++) {
                                    $('#ToDelete[data-id="' + subscriberIdsToDelete[i] + '"]').parent(),parent().remove();
                                }
                                if ($('table > tbody > tr').length <= 0) {
                                    $('table > tbody').append('<tr>' +
                                        '<td colspan="5" class="text-center" id="noDataRow">No data</td>' +
                                        '</tr>');
                                }
                                showNotification('success', 'Delete success!');
                            }
                        });
                    } else {
                        showNotification('info', 'Check subscribers at first!');
                    }
                }
            }
        });
        

    });

    function objectifyForm(formArray) {//serialize data function
        
        var returnArray = {};
        for (var i = 0; i < formArray.length; i++) {
            
            //if (formArray[i]['name'] !== '__RequestVerificationToken') {
                returnArray[formArray[i]['name']] = formArray[i]['value'];
            //}
            
        }
        return returnArray;
    }

    function showNotification(type, message) {
        $.notify({
            // options
            icon: 'glyphicon glyphicon-warning-sign',
            message: message
        }, {
            // settings
            type: type,
            placement: {
                from: "top",
                align: "center"
            },
            offset: 20,
            delay: 3000,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            }
        });
    }
});