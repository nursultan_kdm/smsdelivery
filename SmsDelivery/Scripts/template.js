﻿jQuery(function () {
    $("#templateCreateModalForm").on("submit", function (event) {
        event.preventDefault();
        var url = $(this).data('url');
        var formData = objectifyForm($(this).serializeArray());
        $.ajax({
            type: 'POST',
            url: url,
            data: $(this).serialize(),
            dataType: "json",
            success: function (result) {
                if (result.Success) {
                    var templateType =
                        $("#TemplateTypeId option[value=" + formData.TemplateTypeId + "]").text();
                    var newRow = "<tr>" +
                        "<td><input data-id=" + result.Body.Id + " id=\"ToDelete\" name=\"ToDelete\" type=\"checkbox\" value=\"true\"></td>" +
                        "<td>" + formData.Name + "</td>" +
                        "<td>" + templateType + "</td></tr>";
                    if ($('#noDataRow').length) {
                        $('#noDataRow').parent().remove();
                    }
                    $("tbody").append(newRow);

                    $("#templateCreateModal").modal('hide');

                } else {
                    if (result.Errors.length > 0) {
                        for (var i = 0; i < result.Errors.length; i++) {
                            $("#validationErrors").find("ul").append("<li>" + result.Errors[i] + "</li>");
                        }
                        $("#validationErrors").show();
                    }
                }
            }
        });

    });

    $('.delete-checked-templates').click(function () {
        var url = $(this).data('url');
        var templateIdsToDelete = [];
        var trs = $('table > tbody > tr');
        $('table > tbody > tr #ToDelete:checked').each(function () {
            templateIdsToDelete.push($(this).data('id'));
        });
        bootbox.confirm({
            title: "Subscribers delete",
            message: "Are you sure?",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                if (result) {
                    if (templateIdsToDelete.length > 0) {
                        $.post(url, { ids: templateIdsToDelete }, function (result) {
                            if (result.Success) {
                                for (var i = 0; i < templateIdsToDelete.length; i++) {
                                    $('#ToDelete[data-id="' + templateIdsToDelete[i] + '"]').parent(), parent().remove();
                                }
                                if ($('table > tbody > tr').length <= 0) {
                                    $('table > tbody').append('<tr>' +
                                        '<td colspan="5" class="text-center" id="noDataRow">No data</td>' +
                                        '</tr>');
                                }
                                showNotification('success', 'Delete success!');
                            }
                        });
                    } else {
                        showNotification('info', 'Check templates at first!');
                    }
                }
            }
        });


    });

    function objectifyForm(formArray) {//serialize data function

        var returnArray = {};
        for (var i = 0; i < formArray.length; i++) {

            //if (formArray[i]['name'] !== '__RequestVerificationToken') {
            returnArray[formArray[i]['name']] = formArray[i]['value'];
            //}

        }
        return returnArray;
    }

    function showNotification(type, message) {
        $.notify({
            // options
            icon: 'glyphicon glyphicon-warning-sign',
            message: message
        }, {
                // settings
                type: type,
                placement: {
                    from: "top",
                    align: "center"
                },
                offset: 20,
                delay: 3000,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                }
            });
    }
});