﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SmsDelivery.Startup))]
namespace SmsDelivery
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
