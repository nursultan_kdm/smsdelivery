﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using SmsDelivery.Model;

namespace SmsDelivery.ViewModels
{
    public class CampaignViewModel
    {
        public CampaignViewModel()
        {
            SubscriberGroupViews = new List<SubscriberGroupViewModel>();
        }
        public string Name { get; set; }
        public int? TemplateId { get; set; }
        public string Sender { get; set; }
        public string Body { get; set; }
        public bool CreateSenderName { get; set; }
        public int SaveSenderNameId { get; set; }
        public bool ScheduleDate { get; set; }
        public DateTime? ScheduledDate { get; set; }
        public List<SubscriberGroupViewModel> SubscriberGroupViews { get; set; }
    }
}