﻿namespace SmsDelivery.ViewModels
{
    public class PaymentInstrumentViewModel
    {
        public int Id { get; set; }
        public string Account { get; set; }
        public bool IsMain { get; set; }
    }
}