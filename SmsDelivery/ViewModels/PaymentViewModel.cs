﻿using System;

namespace SmsDelivery.ViewModels
{
    public class PaymentViewModel
    {
        public string PaymentDateStr { get; set; }
        public DateTime PaymentDate { get; set; }
        public decimal Amount { get; set; }
        public string PaymentMethod { get; set; }
        public string Details { get; set; }
        public string Status { get; set; }
    }
}