﻿using System.Collections.Generic;
using System.Web.Mvc;
using SmsDelivery.Model;

namespace SmsDelivery.ViewModels
{
    public class SubscriberGroupViewModel
    {
        public int SubscriberGroupId { get; set; }
        public SelectList SubscriberGroups { get; set; }
    }
}